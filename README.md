# ics-ans-role-prometheus

Ansible role to install prometheus.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-prometheus
```

## License

BSD 2-clause
