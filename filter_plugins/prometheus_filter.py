from cerberus import Validator


class FilterModule(object):
    def filters(self):
        return {
            "csentry_to_file_sd": self.csentry_to_file_sd,  # old filter, requires to loop multiple times on the hostvars
            "csentry_to_prometheus": self.csentry_to_prometheus,
        }

    def csentry_to_file_sd(
        self,
        my_hostvars,
        filter_by_group=None,
        filter_by_domain=None,
        port=None,
        filter_by_device_type=None,
        filter_by_exporter=None,
        exclude_by_group=None,
    ):
        ret = []
        for hostvar in my_hostvars.values():
            labels = {}
            if hostvar.get("csentry_interfaces", []) == []:
                continue
            if filter_by_exporter is not None:
                if filter_by_exporter == "node":
                    if (
                        hostvar.get("ansible_local", {})
                        .get("nodeexporter", {})
                        .get("state", None)
                        != "installed"
                    ):
                        continue
            if filter_by_group is not None and filter_by_group not in hostvar.get(
                "group_names"
            ):
                continue
            if exclude_by_group is not None and exclude_by_group in hostvar.get(
                "group_names"
            ):
                continue
            if (
                filter_by_device_type is not None
                and filter_by_device_type not in hostvar.get("csentry_device_type")
            ):
                continue
            for interface in hostvar.get("csentry_interfaces", []):
                if interface.get("is_main") is True:
                    labels["csentry_domain"] = interface.get("network").get("domain")
                    labels["csentry_vlan"] = (
                        interface.get("network")
                        .get("name")
                        .replace("-", "_")
                        .replace(".", "_")
                    )
                    labels["csentry_vlan_id"] = str(
                        interface.get("network").get("vlan_id")
                    )
                    if (
                        filter_by_domain is not None
                        and labels["csentry_domain"] != filter_by_domain
                    ):
                        break
                    labels["csentry_device_type"] = hostvar.get("csentry_device_type")
                    for group in hostvar.get("group_names"):
                        if group != "all" and group != "ungrouped":
                            label = (
                                "csentry_group_{}".format(group)
                                .replace("-", "_")
                                .replace(".", "_")
                            )
                            labels[label] = "1"
                    if hostvar.get("csentry_is_ioc"):
                        labels["csentry_is_ioc"] = "1"
                    my_hostname = "{}.{}".format(
                        interface.get("name"), interface.get("network").get("domain")
                    )
                    if port is not None:
                        my_hostname = my_hostname + ":{}".format(port)
                    ret.append({"targets": [my_hostname], "labels": labels})
                    break
        return ret

    def csentry_to_prometheus(self, my_hostvars, filters=[]):
        self.validate_filters(filters)
        ret = {}
        for filter in filters:
            ret[filter["name"]] = []
        for single_host in my_hostvars:
            parsed = self.parse_node(my_hostvars[single_host])
            for filter in filters:
                port = filter.get("force_port", None)
                if port is not None:
                    port_append = ":{}".format(port)
                else:
                    port_append = ""
                use_cnames = filter.get("use_cnames", False)
                if parsed is not None and self.filterMatches(parsed, filter):
                    if use_cnames and len(parsed.get("cnames", [])) > 0:
                        for cname in parsed.get("cnames"):
                            ret[filter["name"]].append(
                                {
                                    "targets": ["{}{}".format(cname, port_append)],
                                    "labels": parsed["labels"],
                                }
                            )
                    else:
                        ret[filter["name"]].append(
                            {
                                "targets": [
                                    "{}{}".format(parsed["hostname"], port_append)
                                ],
                                "labels": parsed["labels"],
                            }
                        )
        for _filter in filters:
            if _filter.get("min_host_count") is not None and _filter.get("min_host_count") > len(ret[_filter.get("name")]):
             raise Exception(f'Not enough hosts to proceed in filter {_filter.get("name")}: min={_filter.get("min_host_count")}, got {len(ret[_filter.get("name")])}')
        return ret

    def filterMatches(self, node, afilter):
        exclude_g = afilter.get("exclude_by_group", None)
        if exclude_g is not None and exclude_g in node["groups"]:
            return False
        include_g = afilter.get("by_group", None)
        if include_g is not None and include_g not in node["groups"]:
            return False
        domain = afilter.get("by_domain", None)
        if domain is not None and node["labels"]["csentry_domain"] != domain:
            return False
        device_type = afilter.get("by_device_type", None)
        if (
            device_type is not None
            and node["labels"]["csentry_device_type"] != device_type
        ):
            return False
        exporter = afilter.get("by_exporter", None)
        if exporter is not None and exporter not in node["exporters"]:
            return False
        return True

    def parse_node(self, node):
        parsed = {}
        parsed["labels"] = {}
        parsed["groups"] = []
        if node.get("csentry_interfaces", []) == []:
            return None
        parsed["exporters"] = []
        if (
            node.get("ansible_local", {}).get("nodeexporter", {}).get("state", None)
            == "installed"
        ):
            parsed["exporters"].append("nodeexporter")
        if (
            node.get("ansible_local", {}).get("cadvisor", {}).get("state", None)
            == "installed"
        ):
            parsed["exporters"].append("cadvisor")
        got_a_main_interface = False
        for interface in node.get("csentry_interfaces"):
            if interface.get("is_main", False) is False:
                continue
            got_a_main_interface = True
            network = interface.get("network", None)
            if network is None:
                continue
            domain = network.get("domain")
            parsed["labels"]["csentry_domain"] = domain
            parsed["labels"]["csentry_vlan"] = (
                network.get("name").replace("-", "_").replace(".", "_")
            )
            parsed["labels"]["csentry_vlan_id"] = str(network.get("vlan_id"))
            parsed["labels"]["csentry_device_type"] = node.get("csentry_device_type")
            for group in node.get("group_names"):
                if group != "all" and group != "ungrouped":
                    label = (
                        "csentry_group_{}".format(group)
                        .replace("-", "_")
                        .replace(".", "_")
                    )
                    parsed["labels"][label] = "1"
                    parsed["groups"].append(group)
            if node.get("csentry_is_ioc"):
                parsed["labels"]["csentry_is_ioc"] = "1"
            parsed["hostname"] = "{}.{}".format(interface.get("name"), domain)
            parsed["cnames"] = []
            for cname in interface.get("cnames", []):
                parsed["cnames"].append("{}.{}".format(cname, domain))
        if not got_a_main_interface:
            return None
        return parsed

    def validate_filters(self, filters):
        schema = {
            "name": {"type": "string", "required": True},
            "force_port": {"type": "integer"},
            "by_group": {"type": "string"},
            "exclude_by_group": {"type": "string"},
            "by_domain": {"type": "string"},
            "by_device_type": {"type": "string"},
            "by_exporter": {"type": "string"},
            "use_cnames": {"type": "boolean"},
            "min_host_count": {"type": "integer"}
        }
        validator = Validator(schema)
        for filter in filters:
            if not validator(filter):
                raise Exception("Malformed filters: {}".format(filter))


if __name__ == "__main__":
    filters = [
        {"name": "Test1"},
        {"name": "Test2", "by_group": "nobody"},
        {"by_group": "nobodylovesme"},
    ]
    f = FilterModule()
    f.validate_filters(filters)
